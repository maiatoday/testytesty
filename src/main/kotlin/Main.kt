// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.desktop.Window
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextLayoutInput
import androidx.compose.ui.unit.dp
import components.ConfettiShape
import components.RainbowText
import components.confetti
import theme.SkittlesRainbow

@ExperimentalAnimationApi
fun main() = Window {

    MaterialTheme {
        MainScreenOcto()
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun OldMain() {
    var text by remember { mutableStateOf("") }
    var showConfetti by remember { mutableStateOf(false) }

    MaterialTheme {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .confetti(
                    contentColors = SkittlesRainbow,
                    confettiShape = ConfettiShape.Circle,
                    speed = 0.5F,
                    populationFactor = 0.75f,
                    isVisible = showConfetti
                )
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                OutlinedTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    value = text,
                    singleLine = true,
                    onValueChange = { text = it },
                    label = {
                        Text(
                            "Rainbow text",
                            color = MaterialTheme.colors.secondary
                        )
                    },
                    colors = TextFieldDefaults.outlinedTextFieldColors(focusedBorderColor = MaterialTheme.colors.secondary),
                )
                Button(modifier = Modifier.padding(16.dp),
                    onClick = {
                        showConfetti = !showConfetti
                    }) {
                    Text(text = "Show!", modifier = Modifier.padding(8.dp))
                }
                RainbowText(text = text, style = MaterialTheme.typography.h3)
            }
        }
    }
}