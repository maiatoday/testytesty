package pure.confetti

import androidx.compose.ui.unit.IntSize
import pure.confetti.Confetto.Companion.next
import kotlin.math.roundToInt

//@Parcelize
data class ConfettiState(
    val confetti: List<Confetto> = emptyList(),
    val colorCount: Int,
    val size: SizeParcel = SizeParcel.fromIntSize(IntSize.Zero),
    val speed: Float
) {//: Parcelable {

    companion object {
        fun ConfettiState.sizeChanged(
            size: IntSize,
            populationFactor: Float
        ): ConfettiState {
            if (size == this.size.toIntSize()) return this
            val newSize = SizeParcel.fromIntSize(size)
            return copy(
                confetti = (0..size.realPopulation(populationFactor)).map {
                    Confetto.create(newSize, colorCount)
                },
                size = newSize
            )
        }

        fun ConfettiState.next(durationMillis: Long): ConfettiState {
            return copy(
                confetti = confetti.map {
                    it.next(size, durationMillis, speed)
                }
            )
        }

        fun ConfettiState.populationControl(populationFactor: Float): ConfettiState {
            val count = size.realPopulation(populationFactor = populationFactor)

            return if (count < confetti.size) {
                copy(confetti = confetti.shuffled().take(count))
            } else {
                copy(confetti = confetti + (0..count - confetti.size).map {
                    Confetto.create(
                        size,
                        colorCount
                    )
                })
            }
        }

        private fun IntSize.realPopulation(populationFactor: Float): Int {
            return (width * height / 10_000 * populationFactor).roundToInt()
        }
    }
}

//@Parcelize
data class SizeParcel(val width: Int, val height: Int) {//: Parcelable {
    fun realPopulation(populationFactor: Float): Int {
        return (width * height / 10_000 * populationFactor).roundToInt()
    }

    fun toIntSize(): IntSize = IntSize(width, height)

    companion object {
        fun fromIntSize(intSize: IntSize) = SizeParcel(intSize.width, intSize.height)
    }
}