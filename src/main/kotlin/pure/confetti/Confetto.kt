package pure.confetti

import androidx.compose.ui.geometry.Offset

//@Parcelize
data class Confetto(
    val position: Point,
    val vector: Point,
    val colorIndex: Int,
    val radius: Float,
) {//}: Parcelable {
    companion object {

        fun Confetto.next(
            borders: SizeParcel,
            durationMillis: Long,
            speedCoefficient: Float
        ): Confetto {
            val speed = vector * speedCoefficient

            return Confetto(
                position = position + Point(
                    x = speed.x / 1000f * durationMillis,
                    y = speed.y / 1000f * durationMillis,
                ),
                vector = vector,
                colorIndex = colorIndex,
                radius = radius
            ).let { (position, vector) ->
                val borderTop = 0
                val borderLeft = 0
                val borderBottom = borders.height
                val borderRight = borders.width
                Confetto(
                    position = Point(
                        x = when {
                            position.x < borderLeft -> borderLeft - (position.x - borderLeft)
                            position.x > borderRight -> borderRight - (position.x - borderRight)
                            else -> position.x
                        },
                        y = when {
                            position.y < borderTop -> borderTop - (position.y - borderTop)
                            position.y > borderBottom -> borderBottom - (position.y - borderBottom)
                            else -> position.y
                        }
                    ),
                    vector = Point(
                        x = when {
                            position.x < borderLeft -> -vector.x
                            position.x > borderRight -> -vector.x
                            else -> vector.x
                        },
                        y = when {
                            position.y < borderTop -> -vector.y
                            position.y > borderBottom -> -vector.y
                            else -> vector.y
                        }
                    ),
                    colorIndex = colorIndex,
                    radius = radius
                )
            }
        }

        fun create(borders: SizeParcel, colorCount: Int): Confetto {
            return Confetto(
                position = Point(
                    (0..borders.width).random().toFloat(),
                    (0..borders.height).random().toFloat()
                ),
                vector = Point(
                    // First, randomize direction. Second, randomize amplitude of speed vector.
                    listOf(
                        -1f,
                        1f
                    ).random() * ((borders.width.toFloat() / 100f).toInt()..(borders.width.toFloat() / 10f).toInt()).random()
                        .toFloat(),
                    listOf(
                        -1f,
                        1f
                    ).random() * ((borders.height.toFloat() / 100f).toInt()..(borders.height.toFloat() / 10f).toInt()).random()
                        .toFloat()
                ),
                colorIndex = (0 until colorCount).random(),
                radius = (5..25).random().toFloat()
            )
        }
    }

    //@Parcelize
    data class Point(val x: Float, val y: Float) {//}: Parcelable {
        fun toOffset(): Offset = Offset(x, y)
        operator fun plus(other: Point): Point = Point(x + other.x, y + other.y)
        operator fun minus(other: Point): Point = Point(x - other.x, y - other.y)
        operator fun times(operand: Float): Point = Point(x * operand, y * operand)
    }
}
