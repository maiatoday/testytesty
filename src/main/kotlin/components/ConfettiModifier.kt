package components

import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import pure.confetti.ConfettiState
import pure.confetti.ConfettiState.Companion.next
import pure.confetti.ConfettiState.Companion.populationControl
import pure.confetti.ConfettiState.Companion.sizeChanged

fun Modifier.confetti(
    contentColors: List<Color> = listOf(Color.White),
    confettiShape: ConfettiShape,
    speed: Float,
    populationFactor: Float,
    isVisible: Boolean
) = composed {
    var confettiState by remember {
        mutableStateOf(
            ConfettiState(
                speed = speed,
                colorCount = contentColors.size
            )
        )
    }

    LaunchedEffect(speed, populationFactor, isVisible) {
        confettiState = confettiState.copy(
            speed = speed
        ).populationControl(populationFactor)
    }

    LaunchedEffect(isVisible) {
        var lastFrame = 0L
        while (isActive) {
            delay(60)
           // val nextFrame = awaitFrame() / 100_000L
//            if (lastFrame != 0L) {
//                val period = nextFrame - lastFrame
                confettiState = confettiState.next(200)
//            }
//            lastFrame = nextFrame
        }
    }

    onSizeChanged {
        confettiState = confettiState.sizeChanged(
            size = it,
            populationFactor = populationFactor
        )
    }.drawBehind {
        if (isVisible) {
            confettiState.confetti.forEach { confetto ->
                val color = contentColors[confetto.colorIndex]
                when (confettiShape) {
                    ConfettiShape.Circle -> {
                        drawCircle(color, radius = confetto.radius, center = confetto.position.toOffset())
                    }
                    ConfettiShape.Rectangle -> {
                        drawRect(
                            color,
                            size = Size(confetto.radius, confetto.radius),
                            topLeft = confetto.position.toOffset()
                        )
                    }
                }
            }
        }
    }
}

enum class ConfettiShape {
    Rectangle,
    Circle
}
