package components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import readInput

fun part1(input: List<String>): Long {
    val octoMap = Day11.parse(input)
    var flashes = 0L
    val steps = 100
    for (s in 1..steps) {
        Day11.energyUp(octoMap)
        flashes += Day11.flashCountReset(octoMap)
    }
    return flashes
}

@Composable
fun MainScreenOcto() {
    var octoMapFlat by remember {
        mutableStateOf(Day11.inputFlat())
    }

    Surface(modifier = Modifier.fillMaxSize(), color = Color.Black) {
        LaunchedEffect(null) {
            var octoMap = Day11.parse(readInput("Day11"))
            var flashes = 0L
            val steps = 100
            for (s in 1..steps) {
                Day11.energyUp(octoMap)
                flashes += Day11.flashCountReset(octoMap)
                octoMapFlat = octoMap.flatten()
                delay(200)
            }
        }

        GlowGrid(octoMapFlat, 10)

    }
}

@Composable
fun GlowGrid(octoMapFlat: List<Int>, width:Int) {
    val octoMap = octoMapFlat.chunked(width)
    Column {
        octoMap.map {
            Row {
                it.forEach {
                    Box(modifier = Modifier.size(24.dp).background(energyToColor(Color.White, it)))
                }
            }
        }
    }
}

fun energyToAlpha(energy: Int): Float =
    when {
        (energy == 0) -> 1.0f
        else -> energy.toFloat()/10.0f
    }

fun energyToColor(color: Color, energy: Int): Color = color.copy(alpha = energyToAlpha(energy))





